@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<div class=page-header>
<h1>
    Busqueda
    {{ Form::open(['Route' => 'datos','method' => 'GET', 'class' => 'form-inline pull-right']) }}
        <div class="form-group">
            {{ Form::select('mes', array('' => 'Seleccione mes', '1' => 'Enero', '2' => 'Febrero', '3' => 'Marzo', '4' => 'Abril', '5' => 'Mayo', '6' => 'Junio','7' => 'Julio', '8' => 'Agosto', '9' => 'Septiembre', '10' => 'Octubre', '11' => 'noviembre', '12' => 'Diciembre'), null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::select('sexo', array('' => 'Seleccione sexo','hombre' => 'Hombre', 'Mujer' => 'Mujer'), null ,['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::text('region', null, ['class' => 'form-control', 'placeholder' => 'Regíon']) }}
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">
                <span >Filtrar</span>
            </button>
        </div>
    {{ Form::close() }}
</h1>
</div>
<div>
@if(isset($datos))
@foreach($datos as $key => $dato)
    {{ $key }}
    <table class="table table-hover table-striped">
        <thead>
            <td>Region</td>
            <td>Casos nuevos sin síntomas* </td>
            <td>Fallecidos</td>
            <td>Hombre</td>
            <td>Mujer</td>
        </thead>
        <tbody>
            @foreach($dato as $o => $dat)
            {{-- $o --}}
            <tr>
                <td>{{ $dat->nombre }}</td> 
                <td>{{ $dat->hombre + $dat->Mujer }}</td> 
                <td>{{ $dat->fallecidos }}</td> 
                <td>{{ $dat->hombre }}</td> 
                <td>{{ $dat->Mujer }}</td> 
            </tr>
            @endforeach
        </tbody>
    </table>
@endforeach
@endif
<div>

<div id="SexoRegion">
            {!! $usersChartSexoRegion->container() !!}
        </div>
        {!! $usersChartSexoRegion->script() !!}
</div>
<div id="Region">
            {!! $usersChartRegion->container() !!}
        </div>
        {!! $usersChartRegion->script() !!}
</div>