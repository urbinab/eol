<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datos extends Model
{
    protected $table = 'datos';

    public function scopeFiltroRegion($query, $filtroDatos){
        if($filtroDatos){
            return $query->where('nombre', 'LIKE' ,"%$filtroDatos%");
        }
    }
    public function scopeFiltroSexoRegion($query, $filtroSexo,$FiltroRegion){
        if($FiltroRegion){
            return $query->where('nombre','LIKE',"%$FiltroRegion%");
        }
        if($filtroSexo){
            return $query->select($filtroSexo,'created_at','nombre','region_id');
        }
    }
    public function scopeFiltroMes($query, $filtroDatos){
        if($filtroDatos){
            return $query->orWhereRaw('MONTH(created_at) = ?',[$filtroDatos]);
        }
    }
}
