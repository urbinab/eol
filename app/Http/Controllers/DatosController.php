<?php

namespace App\Http\Controllers;

use App\Datos;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as Collection;
use App\Charts\UserChart;
use Charts;

class DatosController extends Controller
{
    public function index(Request $request)
    {
        $mes = $request->get('mes');
        $sexo = $request->get('sexo');
        $region = $request->get('region');
        
        $datos = Datos::join('region', 'region.id', '=', 'datos.region_id')
                        ->FiltroMes($mes)                
                        ->orderBy('created_at','DESC')
                        ->orderBy('region_id','ASC')
                        ->get();
        $result = array();
        foreach($datos as $t) {
                $result[$t->created_at->format('M')][$t->region_id] = $t;
        }
        $datos = $result;
        
        $usersChartSexoRegion = $this->ChartSexoRegion($sexo,$region);
        $usersChartRegion = $this->ChartRegion($region);
        return view('index',compact('usersChartSexoRegion','datos','usersChartRegion') );
    }

    protected function ChartSexoRegion($sexo,$region){
        $datos = Datos::join('region', 'region.id', '=', 'datos.region_id')
        ->FiltroSexoRegion($sexo,$region)
        ->whereRaw("created_At >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)")
        ->get();
        foreach($datos as $t) {
            $total = 0;
            if($sexo != ''){
                $total += $t->$sexo;
            }else{
                $total += $t->hombre + $t->Mujer;
            }
            $result[$t->created_at->format('M')][$t->region_id] = $total;
            $mes[] = $t->created_at->format('M');
        }
        foreach ($result as $key => $value) {
            $total = 0;
            foreach ($value as $sumatoria){
                $total += $sumatoria;
            }
            $result[$key] = $total;
        }
        
        $mes = array_unique($mes);
        $usersChartSexoRegion = new UserChart;
        $usersChartSexoRegion->labels( $mes );
        $sexo = ($sexo != '')? $sexo : "Ambos sexos";
        $region = ($region != '')? ' de '.$region : " de todas las regiones";
        $usersChartSexoRegion->dataset($sexo.$region , 'bar',  [ $result[ $mes[0] ],$result[ $mes[1] ],$result[ $mes[2] ] ]);
        return $usersChartSexoRegion;
    }
    protected function ChartRegion($region){
        $datos = Datos::join('region', 'region.id', '=', 'datos.region_id')
        ->FiltroRegion($region)
        ->whereRaw("created_At >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)")
        ->orderBy('created_at','DESC')
        ->orderBy('region_id','ASC')
        ->get();
        $total = 0;
        foreach($datos as $t) {
            $result[$t->created_at->format('M')][$t->region_id] = $t->fallecidos;
            $mes[] = $t->created_at->format('M');
        }
        foreach ($result as $key => $value) {
            $total = 0;
            foreach ($value as $sumatoria){
                $total += $sumatoria;
            }
            $result[$key] = $total;
        }
        $mes = array_values(array_unique($mes));
        
        $usersChartRegion = new UserChart;
        $usersChartRegion->labels( $mes );
        $region = ($region != '')? $region : "Todas las regiones";
        $usersChartRegion->dataset($region , 'line',  [ $result[ $mes[0] ],$result[ $mes[1] ],$result[ $mes[2] ] ]);
        return $usersChartRegion;
    }
}
