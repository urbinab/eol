<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fallecidos');
            $table->string('hombre');
            $table->string('Mujer');
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('region');
            $table->index('hombre');
            $table->index('Mujer');
            $table->index('fallecidos');
            $table->index('region_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos');
    }
}
