<?php

use Illuminate\Database\Seeder;

class CargaDatosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('region')->insert([
            'nombre' => 'Arica y Parinacota'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Tarapacá'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Antofagasta'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Atacama'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Coquimbo'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Valparaíso'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Metropolitana'
        ]);
        DB::table('region')->insert([
            'nombre' => 'O’Higgins'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Maule'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Ñuble'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Biobío'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Araucanía'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Los Ríos'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Los Lagos'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Aysén'
        ]);
        DB::table('region')->insert([
            'nombre' => 'Magallanes'
        ]);

        //---- MAYO
        DB::table('datos')->insert([
            'fallecidos' => 1,
            'hombre' => 185,
            'Mujer' => 123,
            'region_id' => 1,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 60,
            'hombre' => 163,
            'Mujer' => 109,
            'region_id' => 2,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 61,
            'hombre' => 444,
            'Mujer' => 296,
            'region_id' => 3,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 8,
            'hombre' => 40,
            'Mujer' => 27,
            'region_id' => 4,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 10,
            'hombre' => 53,
            'Mujer' => 35,
            'region_id' => 5,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 24,
            'hombre' => 383,
            'Mujer' => 255,
            'region_id' => 6,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 995,
            'hombre' => 7594,
            'Mujer' => 5062,
            'region_id' => 7,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 5,
            'hombre' => 75,
            'Mujer' => 50,
            'region_id' => 8,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 1,
            'hombre' => 238,
            'Mujer' => 159,
            'region_id' => 9,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 10,
            'hombre' => 472,
            'Mujer' => 314,
            'region_id' => 10,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 8,
            'hombre' => 455,
            'Mujer' => 304,
            'region_id' => 11,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 14,
            'hombre' => 797,
            'Mujer' => 531,
            'region_id' => 12,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 2,
            'hombre' => 119,
            'Mujer' => 79,
            'region_id' => 13,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 4,
            'hombre' => 310,
            'Mujer' => 207,
            'region_id' => 14,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 0,
            'hombre' => 4,
            'Mujer' => 3,
            'region_id' => 15,
            'created_at' => date('2020-05-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 25,
            'hombre' => 466,
            'Mujer' => 311,
            'region_id' => 16,
            'created_at' => date('2020-05-01')
        ]);

        //---- ABRIL

        DB::table('datos')->insert([
            'fallecidos' => 1,
            'hombre' => 148,
            'Mujer' => 99,
            'region_id' => 1,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 48,
            'hombre' => 131,
            'Mujer' => 87,
            'region_id' => 2,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 49,
            'hombre' => 355,
            'Mujer' => 237,
            'region_id' => 3,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 6,
            'hombre' => 32,
            'Mujer' => 21,
            'region_id' => 4,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 8,
            'hombre' => 42,
            'Mujer' => 28,
            'region_id' => 5,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 19,
            'hombre' => 306,
            'Mujer' => 204,
            'region_id' => 6,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 796,
            'hombre' => 6075,
            'Mujer' => 4040,
            'region_id' => 7,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 4,
            'hombre' => 60,
            'Mujer' => 40,
            'region_id' => 8,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 1,
            'hombre' => 191,
            'Mujer' => 127,
            'region_id' => 9,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 8,
            'hombre' => 377,
            'Mujer' => 252,
            'region_id' => 10,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 6,
            'hombre' => 364,
            'Mujer' => 243,
            'region_id' => 11,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 11,
            'hombre' => 637,
            'Mujer' => 425,
            'region_id' => 12,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 2,
            'hombre' => 95,
            'Mujer' => 63,
            'region_id' => 13,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 3,
            'hombre' => 248,
            'Mujer' => 165,
            'region_id' => 14,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 0,
            'hombre' => 3,
            'Mujer' => 2,
            'region_id' => 15,
            'created_at' => date('2020-04-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 20,
            'hombre' => 373,
            'Mujer' => 249,
            'region_id' => 16,
            'created_at' => date('2020-04-01')
        ]);


        //---- MARZO

        DB::table('datos')->insert([
            'fallecidos' => 1,
            'hombre' => 11,
            'Mujer' => 74,
            'region_id' => 1,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 36,
            'hombre' => 98,
            'Mujer' => 65,
            'region_id' => 2,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 37,
            'hombre' => 266,
            'Mujer' => 178,
            'region_id' => 3,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 5,
            'hombre' => 24,
            'Mujer' => 16,
            'region_id' => 4,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 6,
            'hombre' => 32,
            'Mujer' => 21,
            'region_id' => 5,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 14,
            'hombre' => 230,
            'Mujer' => 153,
            'region_id' => 6,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 597,
            'hombre' => 4556,
            'Mujer' => 3037,
            'region_id' => 7,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 3,
            'hombre' => 45,
            'Mujer' => 30,
            'region_id' => 8,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 1,
            'hombre' => 143,
            'Mujer' => 95,
            'region_id' => 9,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 6,
            'hombre' => 283,
            'Mujer' => 189,
            'region_id' => 10,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 5,
            'hombre' => 273,
            'Mujer' => 182,
            'region_id' => 11,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 8,
            'hombre' => 478,
            'Mujer' => 319,
            'region_id' => 12,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 1,
            'hombre' => 71,
            'Mujer' => 48,
            'region_id' => 13,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 2,
            'hombre' => 186,
            'Mujer' => 124,
            'region_id' => 14,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 0,
            'hombre' => 3,
            'Mujer' => 2,
            'region_id' => 15,
            'created_at' => date('2020-03-01')
        ]);
        DB::table('datos')->insert([
            'fallecidos' => 15,
            'hombre' => 280,
            'Mujer' => 186,
            'region_id' => 16,
            'created_at' => date('2020-03-01')
        ]);
    }
}
